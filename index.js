/*jslint node: true, sloppy: true, white: true*/
var app = angular.module('trojanpreneurapp', ['ngMaterial']);

app.controller('AppCtrl', ['$scope', '$http', '$location', '$mdSidenav', function($scope, $http, $location, $mdSidenav){

  
  
  console.log('test');

}]);


app.config(function($mdThemingProvider) {

  $mdThemingProvider.definePalette('cardinal', {
    '50': '000000',
    '100': '260000',
    '200': '400000',
    '300': '590000',
    '400': '730000',
    '500': '8c0000',
    '600': '990000', //This is the cardinal color
    '700': 'a60d0d',
    '800': 'bf2626',
    '900': 'd94040',
    'A100': 'f25959',
    'A200': 'ff7373',
    'A400': 'ffa6a6',
    'A700': 'ffffff',
    'contrastDefaultColor': 'light',    // whether, by default, text (contrast)
    // on this palette should be dark or light
    'contrastDarkColors': ['50', '100', //hues which contrast should be 'dark' by default
                           '50', '300', '400', 'A100'],
    'contrastLightColors': undefined    // could also specify this if default was 'dark'
  });

  $mdThemingProvider.definePalette('gold', {
    '50': '592600',
    '100': '8c5900',
    '200': 'a67300',
    '300': 'bf8c00',
    '400': 'd9a600',
    '500': 'f2bf00',
    '600': 'ffcc00', //This is the gold color
    '700': 'ffd90d',
    '800': 'fff226',
    '900': 'ffff40',
    'A100': 'ffff59',
    'A200': 'ffff73',
    'A400': 'ffffa6',
    'A700': 'ffffff',
    'contrastDefaultColor': 'dark',    // whether, by default, text (contrast)
    // on this palette should be dark or light
    'contrastDarkColors': ['50', '100', //hues which contrast should be 'dark' by default
                           '200', '300', '400', 'A100'],
    'contrastLightColors': undefined    // could also specify this if default was 'dark'
  });



  $mdThemingProvider.theme('default')
    .primaryPalette('cardinal', {
    'default': '600',
    'hue-1': '100', // use shade 100 for the <code>md-hue-1</code> class
    'hue-2': '600', // use shade 600 for the <code>md-hue-2</code> class
    'hue-3': 'A100' // use shade A100 for the <code>md-hue-3</code> class
  })
  // If you specify less than all of the keys, it will inherit from the
  // default shades
    .accentPalette('gold', {
    'default': '600' // use shade 200 for default, and keep all other shades the same
  });
});